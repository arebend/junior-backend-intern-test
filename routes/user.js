const router = require("express").Router();
const user = require("../controllers/user");
const authentication = require("../middlewares/authentication");
const rateLimit = require("express-rate-limit");

const createAccountLimiter = rateLimit({
    windowMs: 6 * 5 * 1000, // 30 scnds window
    max: 2, // start blocking after 2 requests
    message: {
        "success": 400,
        message: "Too many request. Please try again later (30s)"
    },
    skipSuccessfulRequests: true,
    draft_polli_ratelimit_headers: true,
    headers: true,
    onLimitReached: (req, res, options) => {
        options.windowMs = 300;
    }
});


router.post("/register", user.register);
router.post("/login", createAccountLimiter, user.login);
router.get("/get", authentication, user.getUser);

module.exports = router;