const router = require("express").Router();
const duplicate = require("../controllers/findSameNumber");

router.post("/number", duplicate.sameNumber);

module.exports = router;