const router = require("express").Router();

const duplicated = require("./duplicated");
const user = require("./user");

router.use("/duplicate", duplicated);
router.use("/user", user);

module.exports = router;