const request = require("supertest");
const app = require("../app");
const db = require("../models");
const {
    User
} = require("../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

let token;

describe("Test User API Collection", () => {

    beforeAll(() => {
        return db.sequelize.query("TRUNCATE \"Users\" RESTART IDENTITY")
            .then(() => {
                return User.create({
                    email: "testerlagi@mail.com",
                    password: bcrypt.hashSync("123456", 10),
                    fullName: "Jhon Doe"
                });
            })
            .then(user => {
                token = jwt.sign({
                    id: user.id,
                    email: user.email,
                    fullName: user.fullName
                }, process.env.SECRET);
            });
    });

    afterAll(() => {
        return Promise.all([
            db.sequelize.query("TRUNCATE \"Users\" RESTART IDENTITY"),
            User.destroy({
                where: {
                    email: "tester@email.com" && "testerlagi@mail.com"
                }
            })
        ]);
    });

    describe("POST /user/register", () => {
        test("Success register. Status code 200", done => {
            request(app)
                .post("/user/register")
                .set("Content-Type", "application/json")
                .send({
                    email: "tester@email.com",
                    password: "123456",
                    fullName: "abdul"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    done();
                });
        });
        test("Failed register. Status code 400", done => {
            request(app)
                .post("/user/register")
                .set("Content-Type", "application/json")
                .send({
                    email: "tester@email.com",
                    password: 2,
                    fullName: "test"
                })
                .then(res => {
                    console.log(res);
                    expect(res.statusCode).toEqual(400);
                    done();
                });
        });
    });

    describe("POST user/login", () => {
        test("Success Login. Status code 200", done => {
            request(app)
                .post("/user/login")
                .set("Content-Type", "application/json")
                .send({
                    email: "testerlagi@mail.com",
                    password: "123456"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    done();
                });
        });
        test("Failed Login. Status code 401", done => {
            request(app)
                .post("/user/login")
                .set("Content-Type", "application/json")
                .send({
                    email: "tester@email.com",
                    password: "12345aas"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    done();
                });
        });
    });

});