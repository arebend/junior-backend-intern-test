module.exports = {
    success: (statusCode, statusmessage, result) => ({
        success: statusCode,
        message: statusmessage,
        result
    }),

    error: (statusCode, err) => ({
        success: statusCode,
        message: err.message
    })

};