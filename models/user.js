"use strict";
const {
    Model
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    User.init({
        fullName: {
            type: DataTypes.STRING,
            allowNull: {
                args: false,
                msg: "Fullname must be filled"
            }
        },
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: {
                    args: true,
                    msg: "Incorrect email format or email should be not empty"
                }
            },
            allowNull: {
                args: false,
                msg: "Email must be filled"
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: {
                args: false,
                msg: "password must be filled"
            }
        }
    }, {
        sequelize,
        modelName: "User",
    });
    return User;
};