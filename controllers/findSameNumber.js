const response = require("../helpers/responseForFind");

module.exports = {

    async sameNumber(req, res) {
        let num = req.body.input;
        console.log(num);
        let input = await req.body.input.split("");
        function countSameNumber(input) {
            let duplicated = [];
            let arr = input;
            for (let i = 0; i < arr.length; i++) {
                let a = arr.filter((v) => v === arr[i]);
                if (a.length > 1) {
                    duplicated.push({ number: arr[i], totalDuplicated: a.length });
                    arr = arr.filter((e) => e != arr[i]);
                }
            }
            return duplicated;
        }
        let result = await countSameNumber(input);
        let final = {
            number: num,
            duplicated: result
        };
        res.status(200).json(
            response.success(200, final));
    }

};