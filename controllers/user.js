const {
    User
} = require("../models");
const response = require("../helpers/responseFormater");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
    async register(req, res) {
        let {
            fullName,
            email,
        } = req.body;
        try {
            let user = await User.create({
                fullName,
                email,
                password: await bcrypt.hashSync(req.body.password, 10)
            });
            let token = jwt.sign({
                id: user.id,
                email: user.email
            }, process.env.SECRET);
            res.status(200).json(
                response.success(200, "Registration success", ({
                    fullName: user.fullName,
                    email: user.email,
                    token: token
                }))
            );
        } catch (err) {
            console.log(err);
            res.status(400).json(
                response.error(400, (new Error("Too many request. Please try again later (30s)")))
            );
        }
    },

    async login(req, res) {
        let {
            email,
            password
        } = req.body;
        try {
            let user = await User.findOne({
                where: {
                    email
                }
            });
            if (!user) {
                res.status(400).json(
                    response.error(400, (new Error({
                        message: "Too many request. Please try again later (30s)"
                    })))
                );
            } else {
                let isValidated = bcrypt.compareSync(password, user.password);
                if (isValidated) {
                    let token = jwt.sign({
                        id: user.id,
                        email: user.email,
                        fullName: user.fullName
                    }, process.env.SECRET);
                    res.status(200).json(
                        response.success(200, "Login Success", ({
                            fullName: user.fullName,
                            email: user.email,
                            token: token
                        }))
                    );
                } else {
                    res.status(401).json(
                        response.error(401, (new Error("Wrong Password")))
                    );
                }
            }
        } catch (err) {
            res.status(400).json(
                response.error(400, err)
            );
        }
    },
    async getUser(req, res) {
        try {
            let profile = await User.findByPk(req.user.id);
            if (profile) {
                res.status(200).json(
                    response.success(200, "Success Get profile", ({
                        fullName: profile.fullName,
                        email: profile.email
                    }))
                );
            } else {
                res.status(403).json(
                    response.error(400, (new Error("You are not allowed")))
                );
            }
        } catch (err) {
            res.status(400).json(
                response.error(400, (new Error("Too many request. Please try again later (30s)")))
            );
        }
    }
};